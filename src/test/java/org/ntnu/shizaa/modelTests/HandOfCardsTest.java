package org.ntnu.shizaa.modelTests;

import org.junit.jupiter.api.Test;
import org.ntnu.shizaa.model.Card;
import org.ntnu.shizaa.model.HandOfCards;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    @Test
    void isFlush() {
        List<Card> flushCards = Arrays.asList(new Card('H', 2), new Card('H', 3), new Card('H', 4), new Card('H', 5), new Card('H', 6));
        HandOfCards flushHand = new HandOfCards(flushCards);
        assertTrue(flushHand.isFlush());

        List<Card> nonFlushCards = Arrays.asList(new Card('H', 2), new Card('S', 3), new Card('H', 4), new Card('H', 5), new Card('H', 6));
        HandOfCards nonFlushHand = new HandOfCards(nonFlushCards);
        assertFalse(nonFlushHand.isFlush());
    }

    @Test
    void isStraight() {
        List<Card> straightCards = Arrays.asList(new Card('H', 2), new Card('S', 3), new Card('H', 4), new Card('D', 5), new Card('C', 6));
        HandOfCards straightHand = new HandOfCards(straightCards);
        assertTrue(straightHand.isStraight());

        List<Card> nonStraightCards = Arrays.asList(new Card('H', 2), new Card('S', 3), new Card('H', 5), new Card('D', 6), new Card('C', 7));
        HandOfCards nonStraightHand = new HandOfCards(nonStraightCards);
        assertFalse(nonStraightHand.isStraight());
    }

    @Test
    void printHand() {
        List<Card> cards = Arrays.asList(new Card('H', 2), new Card('S', 3), new Card('H', 4), new Card('D', 5), new Card('C', 6));
        HandOfCards hand = new HandOfCards(cards);

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        hand.printHand();

        String expectedOutput = "H2\nS3\nH4\nD5\nC6\n";
        assertEquals(expectedOutput, outContent.toString());
    }
}