package org.ntnu.shizaa.modelTests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.ntnu.shizaa.model.Card;

public class CardTest {

    @Test
    @DisplayName("Test Card constructor")
    public void testCardConstructor() {
        Card card = new Card('H', 7);
        assert card.getRank() == 7;
        assert card.getSuit() == 'H';
    }

    @Test
    @DisplayName("Test rankToString method")
    public void testToString() {
        Card card = new Card('H', 7);
        assert card.toString().equals("H7");
    }
}

