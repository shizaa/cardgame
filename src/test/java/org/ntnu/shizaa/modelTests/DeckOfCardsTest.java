package org.ntnu.shizaa.modelTests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.ntnu.shizaa.model.DeckOfCards;

public class DeckOfCardsTest {

    @Test
    @DisplayName("Test DeckOfCards constructor")
    public void testDeckOfCardsConstructor() {
        DeckOfCards deckOfCards = new DeckOfCards();
        assert deckOfCards.cards.size() == 52;
    }

    @Test
    @DisplayName("Test dealHand method")
    public void testDealHand() {
        DeckOfCards deckOfCards = new DeckOfCards();
        assert deckOfCards.dealHand(5).size() == 5;
    }

    @Test
    @DisplayName("Test if dealHand throws exception")
    public void testDealHandException(){
        DeckOfCards deckOfCards = new DeckOfCards();
        try {
            deckOfCards.dealHand(53);
        } catch (IllegalArgumentException e) {
            assert e.getMessage().equals("Not enough cards in the deck");
        }
    }
}
