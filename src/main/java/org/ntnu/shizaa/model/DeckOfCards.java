package org.ntnu.shizaa.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * A class representing a deck of cards. The deck is a standard 52 card deck.
 */

public class DeckOfCards {

    public List<Card> cards;
    private final char[] suits = { 'S', 'H', 'D', 'C' };

    /**
     * Constructor for the DeckOfCards class. Creates a standard 52 card deck.
     */

    public DeckOfCards() {
        cards = new ArrayList<>(52);
        for (char suit : suits) {
            for (int rank = 1; rank <= 13; rank++) {
                cards.add(new Card(suit, rank));
            }
        }
    }

    /**
     * Picks n random cards from the deck and returns these in an ArrayList.
     * @param n the number of cards to pick
     * @return an ArrayList of n cards
     */
    public List<Card> dealHand(int n) {
        if (n > cards.size()) {
            throw new IllegalArgumentException("Not enough cards in the deck");
        }

        Random random = new Random();
        List<Card> hand = random.ints(n, 0, cards.size() - 1)
                .mapToObj(cards::remove)
                .collect(Collectors.toList());

        return hand;
    }


}
