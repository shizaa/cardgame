package org.ntnu.shizaa.model;

public class Card {

    private int rank;
    private char suit;

    public Card(char suit, int rank) {
        this.suit = suit;
        this.rank = rank;

    }

    public int getRank() {
        return rank;
    }

    public int getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return suit + String.valueOf(rank);
    }
}
