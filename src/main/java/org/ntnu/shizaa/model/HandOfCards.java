package org.ntnu.shizaa.model;

import java.util.ArrayList;
import java.util.List;

public class HandOfCards {
    private List<Card> cards;

    public HandOfCards(List<Card> cards) {
        this.cards = new ArrayList<>(cards);
    }

    public void printHand() {
        for (Card card : cards) {
            System.out.println(card);
        }
    }
    public int getSumOfFaces(List<Card> currentHand){
        return currentHand.stream().mapToInt(Card::getRank).sum();
    }

    /**
     * Method that checks if the hand is a flush
     * @param hand the hand of cards
     */

    public boolean isFlush(List<Card> hand) {
        char suit = (char) hand.get(0).getSuit();
        return hand.stream().allMatch(card -> card.getSuit() == suit);
    }

    /**
     * Method that checks if the card queen of clubs is in the hand
     * */

    public boolean isQueenOfClubs(List<Card> currentHand){
        return currentHand.stream().anyMatch(card -> card.getRank() == 12 && card.getSuit() == 'S');
    }

    /**
     * Method that writes the suit and rank of all the hearts in the hand
     */

    public String getCardsOfHearts(List<Card> currentHand){
        List<Card> cardsOfHearts = new ArrayList<>();
        currentHand.stream()
                .filter(card -> card.getSuit() == 'H')
                .forEach(cardsOfHearts::add);

        return cardsOfHearts.toString();
    }

}
