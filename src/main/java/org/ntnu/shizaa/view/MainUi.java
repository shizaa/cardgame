package org.ntnu.shizaa.view;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
//import org.ntnu.shizaa.controller.DisplayOfCardsController;
import org.ntnu.shizaa.model.Card;
import org.ntnu.shizaa.model.DeckOfCards;
import org.ntnu.shizaa.model.HandOfCards;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainUi extends Application {

    public DeckOfCards deckOfCards = new DeckOfCards();
    private HBox displayHBox1 = new HBox();
    public List<Card> currentHand = new ArrayList<>();

    public HandOfCards handOfCards = new HandOfCards(currentHand);
    private Label cardsOfHeartsLabel;
    private Label queenOfSpadesLabel;
    private Label sumOfFacesLabel;
    private Label flushLabel;



    @Override
    public void start(Stage primaryStage) {
        AnchorPane root = new AnchorPane();
        root.setPrefSize(1250, 800);

        root.setBackground(new Background(new BackgroundFill(Color.rgb(5, 48, 7), null, null)));
        root.getStylesheets().add(getClass().getResource("/DisplayOfCards.css").toExternalForm());
        BorderPane borderPane = new BorderPane();
        borderPane.setLayoutX(34);
        borderPane.setLayoutY(42);
        borderPane.setPrefSize(909, 715);
        borderPane.getStyleClass().add("cardDisplay");
        HBox bottomBox = new HBox();
        bottomBox.setPrefSize(200, 100);
        BorderPane.setAlignment(bottomBox, Pos.CENTER);

        HBox dealHandBox = new HBox();
        dealHandBox.setAlignment(Pos.CENTER);
        dealHandBox.setPrefSize(200, 100);
        Button dealHandButton = new Button("Deal Hand");
        dealHandButton.setPrefSize(102, 37);
        dealHandButton.getStyleClass().add("btn");
        dealHandBox.getChildren().add(dealHandButton);
        dealHandButton.setOnAction(event -> dealCards(5));

        Pane spacerPane = new Pane();
        spacerPane.setPrefSize(200, 200);
        HBox.setHgrow(spacerPane, Priority.ALWAYS);

        HBox checkHandBox = new HBox();
        checkHandBox.setAlignment(Pos.CENTER);
        checkHandBox.setPrefSize(200, 100);
        Button checkHandButton = new Button("Check hand");
        checkHandButton.setPrefSize(107, 12);
        checkHandButton.getStyleClass().add("btn");
        checkHandBox.getChildren().add(checkHandButton);
        checkHandButton.setOnAction(event -> {
            sumOfFacesLabel.setText("Sum of the faces: " + handOfCards.getSumOfFaces(currentHand));
            flushLabel.setText(handOfCards.isFlush(currentHand) ? "Yes" : "No");
            queenOfSpadesLabel.setText(handOfCards.isQueenOfClubs(currentHand) ? "Yes" : "No");
            cardsOfHeartsLabel.setText(handOfCards.getCardsOfHearts(currentHand));
        });

        bottomBox.getChildren().addAll(dealHandBox, spacerPane, checkHandBox);
        borderPane.setBottom(bottomBox);

         Text topText = new Text("Your hand");
        topText.setFont(new Font("Georgia", 72));
        BorderPane.setAlignment(topText, Pos.CENTER);
        BorderPane.setMargin(topText, new Insets(20, 0, 0, 0));
        borderPane.setTop(topText);

        VBox centerBox = new VBox();
        centerBox.setPrefSize(835, 471);
        BorderPane.setAlignment(centerBox, Pos.CENTER);
        BorderPane.setMargin(centerBox, new Insets(20));

        displayHBox1.setPrefSize(865, 235);
        VBox.setMargin(displayHBox1, new Insets(70));
        displayHBox1.setSpacing(10);
        displayHBox1.setPadding(new Insets(10));

        centerBox.getChildren().addAll(displayHBox1);
        borderPane.setCenter(centerBox);

        root.getChildren().add(borderPane);

        VBox rightVBox = createInfoVBox("Cards of hearts:", "Queen of spades:", "Sum of the faces:", "Flush:");
        rightVBox.setLayoutX(962);
        rightVBox.setLayoutY(44);
        rightVBox.setPrefSize(271, 709);
        rightVBox.setPadding(new Insets(20, 0, 0, 0));

        root.getChildren().add(rightVBox);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private VBox createInfoVBox(String... titles) {
        VBox vBox = new VBox();
        for (String title : titles) {
            VBox innerVBox = new VBox();
            innerVBox.setAlignment(javafx.geometry.Pos.TOP_CENTER);
            innerVBox.setPrefSize(249, 89);

            Text text = new Text(title);
            text.setFill(javafx.scene.paint.Color.WHITE);
            text.getStyleClass().add("text");
            text.setFont(new Font(22));
            VBox.setMargin(text, new Insets(10, 0, 0, 0));

            Pane pane = new Pane();
            pane.setPrefSize(233, 38);
            pane.setStyle("-fx-background-color: white;");
            VBox.setMargin(pane, new Insets(10, 20, 10, 20));

            Label label = new Label();
            label.setAlignment(javafx.geometry.Pos.CENTER);
            label.setLayoutX(6);
            label.setLayoutY(5);
            label.setPrefSize(221, 26);

            switch (title) {
                case "Cards of hearts:":
                    cardsOfHeartsLabel = label;
                    break;
                case "Queen of spades:":
                    queenOfSpadesLabel = label;
                    break;
                case "Sum of the faces:":
                    sumOfFacesLabel = label;
                    break;
                case "Flush:":
                    flushLabel = label;
                    break;
            }

            pane.getChildren().add(label);
            innerVBox.getChildren().addAll(text, pane);
            VBox.setMargin(innerVBox, new Insets(50, 0, 0, 0));

            vBox.getChildren().add(innerVBox);
        }
        return vBox;
    }
    private void dealCards(int n) {
        try {
            currentHand = deckOfCards.dealHand(n);
            displayHBox1.getChildren().clear();

            for (Card card : currentHand) {
                ImageView cardView = createCardImageView(card);
                HBox.setMargin(cardView, new Insets(10));
                displayHBox1.getChildren().add(cardView);
            }
        } catch (IllegalArgumentException e) {
            displayHBox1.getChildren().clear();
            Text errorMessage = new Text("Not enough cards in the deck");
            errorMessage.setFill(Color.RED);
            errorMessage.setFont(new Font(20));
            displayHBox1.getChildren().add(errorMessage);
        }

    }

    private ImageView createCardImageView(Card card) {
        String imageName = "/img/" + card.toString() + ".png";;
        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream(imageName)));
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(170);
        imageView.setPreserveRatio(true);
        return imageView;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
